from rest_framework import routers
from .views import PostViewSet, CommentViewSet

router = routers.SimpleRouter()
router.register("users/(?P<user_id>\d+)/posts", PostViewSet)
router.register("users/(?P<user_id>\d+)/posts/(?P<post_id>\d+)/comments", CommentViewSet)

urlpatterns = router.urls
