from django.http import HttpResponse
from .models import Post, Comment
from .serializers import PostSerializer, CommentSerializer
from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated
from .permissions import AllowOnlyAuthorToUpdate, AllowOnlyUserOrAuthorToDelete


def home(request):
    posts = Post.objects.all()
    s = PostSerializer(posts, many=True)
    import ipdb
    ipdb.set_trace()
    return HttpResponse(s.data)


class PostViewSet(
                  mixins.RetrieveModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    permission_classes = (
      IsAuthenticated,
      AllowOnlyAuthorToUpdate,
      AllowOnlyUserOrAuthorToDelete)
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def filter_queryset(self, queryset):
        queryset = super(PostViewSet, self).filter_queryset(queryset)
        return queryset.filter(user=self.kwargs["user_id"])


class CommentViewSet(
                  mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()

    def filter_queryset(self, queryset):
        queryset = super(CommentViewSet, self).filter_queryset(queryset)
        return queryset.filter(post__user=self.kwargs["user_id"],
                               post=self.kwargs["post_id"])
