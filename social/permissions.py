from rest_framework import permissions


class AllowOnlyAuthorToUpdate(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, obj):
        if view.action in ["update", "partial_update"]:
            return obj.author == request.user
        return True


class AllowOnlyUserOrAuthorToDelete(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if view.action == "destroy":
            return obj.author == request.user or obj.user == request.user

        return True
