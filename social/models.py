from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    user = models.ForeignKey(User, related_name="posts")
    message = models.CharField(max_length=200)
    author = models.ForeignKey(User, related_name="posts_authored")

    def save(self, *args, **kwargs):
        self.message = self.message.upper()
        super(Post, self).save(*args, **kwargs)

    def get_last_3_comments(self):
        return self.comments.order_by("-id")[:2]


class Comment(models.Model):
    post = models.ForeignKey(Post, related_name="comments")
    body = models.CharField(max_length=200)
    author = models.ForeignKey(User, related_name="comments_posted")
