from rest_framework import serializers
from .models import Post, Comment
from django.contrib.auth.models import User


class CommentSerializer(serializers.ModelSerializer):
    author = serializers.PrimaryKeyRelatedField(
        default=serializers.CurrentUserDefault(),
        read_only=True,
    )

    def create(self, validated_data):
        post_id = self.context["view"].kwargs["post_id"]
        return Comment.objects.create(
            author=validated_data["author"],
            body=validated_data["body"],
            post=Post.objects.get(id=post_id)
            )

    class Meta:
        model = Comment
        fields = ("__all__")
        read_only_fields = ("post", )


class PostSerializer(serializers.ModelSerializer):
    get_last_3_comments = CommentSerializer(many=True)
    author = serializers.PrimaryKeyRelatedField(
        default=serializers.CurrentUserDefault(),
        read_only=True,
    )

    def create(self, validated_data):
        user_id = self.context["view"].kwargs["user_id"]
        validated_data["user"] = User.objects.get(id=user_id)
        post = Post.objects.create(**validated_data)
        return post

    def update(self, instance, validated_data):
        instance.message = validated_data["message"]
        instance.save()
        return instance

    class Meta:
        model = Post
        fields = ("__all__")
        read_only_fields = ("user", )
