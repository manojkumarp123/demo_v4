# Django Rest Framework Demo

## Social Network

### User Profile

1. Posts

    List posts on users profile
    GET /users/{id}/posts/

    Create a post on users profile
    POST /users/{id}/posts/
    {message: "how are you"}

2. Comments

    List all comments on the post
    GET /users/{user_id}/posts/{post_id}/comments/

    Create a comment on a post
    POST /users/{user_id}/posts/{post_id}/comments/
    {body: "nice post"}
